from typing import List

def seperating_query_and_pages(lst:List) -> List[str]:
	page_lst = []
	query_lst = []
	for data in range(len(lst)):
		if lst[data][0:2] == 'P ':
			page_lst.append(lst[data])
		elif lst[data][0:2] == 'Q ':
			query_lst.append(lst[data])
	if  len(page_lst) > 25:
		return page_lst[0:25], query_lst
	return page_lst, query_lst

print(seperating_query_and_pages(['P programming computer', 'Q smalltalk', 'P smalltalk  Programming computer', 'Q programming']))
